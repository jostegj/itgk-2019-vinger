def converting(binary_num):
    decimal_num = 0
    for i in range(len(binary_num)):
        decimal_num += 2**i * int((binary_num[-i-1]))
    return decimal_num


print(converting('101010'))
